#Basic Information
Name:		lxcfs-tools
Version:	0.3
Release:	35
Summary:	toolkit for lxcfs to remount a running isulad
License:    Mulan PSL v2
URL:        https://gitee.com/openeuler/lxcfs-tools
Source0:	https://gitee.com/openeuler/lxcfs-tools/repository/archive/v%{version}.tar.gz
BuildRoot:  %{_tmppath}/%{name}-root

Patch1: 0001-lxcfs-tools-build-security-option.patch
Patch2: 0002-enable-external-linkmode-for-cgo-build.patch
Patch3: 0003-retry-10-times-to-avoid-isulad-unavailable.patch
Patch4: 0004-add-dt-test.patch
Patch5: 0005-add-riscv64-to-syscall-build.patch
Patch6: 0006-lxcfs-tools-support-lxcfs-reliability-for-doc.patch
Patch7: 0007-test-add-tests-for-docker-and-lxcfs.patch

%ifarch sw_64
Patch1000: lxcfs-tools-sw.patch
%endif

#Dependency
BuildRequires:	golang > 1.7
BuildRequires:  glibc-static
Requires: lxcfs

%description
A toolkit for lxcfs to remount a running isulad when crashes recover

#Build sections
%prep
%autosetup -n %{name}-v%{version} -p1
%ifarch loongarch64
sed  -i '/golang.org\/x\/sys =>/d' go.mod
export GOSUMDB="sum.golang.org"
export GOPROXY="https://goproxy.cn"
go get -d golang.org/x/sys@v0.19.0
go mod tidy
go mod download
go mod vendor
# add loong64 to syscall_linux_64.go file
sed 's/mips64/mips64 loong64/g' -i vendor/github.com/opencontainers/runc/libcontainer/system/syscall_linux_64.go
%endif

%build
make

%install
HOOK_DIR=$RPM_BUILD_ROOT/var/lib/isulad/hooks
LXCFS_TOOLS_DIR=$RPM_BUILD_ROOT/usr/local/bin

mkdir -p -m 0700 ${HOOK_DIR}
mkdir -p -m 0700 ${LXCFS_TOOLS_DIR}

install -m 0750 build/lxcfs-hook ${HOOK_DIR}
install -m 0750 build/lxcfs-tools ${LXCFS_TOOLS_DIR}

%check
make tests

#Install and uninstall scripts
%pre

%preun

%post
GRAPH=`isula info 2>/dev/null | grep -Eo "iSulad Root Dir:.+" | grep -Eo "/.*"`
if [ x"$GRAPH" == "x" ]; then
    GRAPH="/var/lib/isulad"
fi

if [[ ("$GRAPH" != "/var/lib/isulad") ]]; then
    mkdir -p -m 0550 $GRAPH/hooks
    install -m 0550 -p /var/lib/isulad/hooks/lxcfs-hook $GRAPH/hooks

    echo
    echo "=================== WARNING! ================================================"
    echo " 'iSulad Root Dir' is $GRAPH, move /var/lib/isulad/hooks/lxcfs-hook to  $GRAPH/hooks"
    echo "============================================================================="
    echo
fi
HOOK_SPEC=${GRAPH}/hooks
HOOK_DIR=${GRAPH}/hooks
touch ${HOOK_SPEC}/hookspec.json
cat << EOF > ${HOOK_SPEC}/hookspec.json
{
        "prestart": [
        {
                "path": "${HOOK_DIR}/lxcfs-hook",
                "args": ["lxcfs-hook"],
                "env": []
        }
        ],
        "poststart":[],
        "poststop":[]
}

EOF
chmod 0640 ${HOOK_SPEC}/hookspec.json

%postun

#Files list
%files
%defattr(0550,root,root,0550)
/usr/local/bin/lxcfs-tools
%attr(0550,root,root) /var/lib/isulad/hooks
%attr(0550,root,root) /var/lib/isulad/hooks/lxcfs-hook

#Clean section
%clean 
rm -rfv %{buildroot}


%changelog
* Tue Apr 16 2024 Pengda Dou <doupengda@loongson.cn> - 0.3-35
- add support for loongarch64

* Fri Feb 2 2024 yangjiaqi <yangjiaqi16@huawei.com> - 0.3-34
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:support lxcfs reliability for docker

* Wed Dec 27 2023 yangjiaqi <yangjiaqi16@huawei.com> - 0.3-33
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:hide error when isula info is abnormal

* Thu Aug 31 2023 yangjiaqi<yangjiaqi16@huawei.com> - 0.3-32
- Type:bugfix
- CVE:NA
- SUG:restart
- DESC:adjust the position of Shenwei patch

* Tue Aug 29 2023 yangjiaqi<yangjiaqi16@huawei.com> - 0.3-31
- Type:bugfix
- CVE:NA
- SUG:restart
- DESC:remove redundant symbol and adjust patches order

* Mon Jul 03 2023 wujie <wujie@nj.iscas.ac.cn> - 0.3-30
- add riscv64 to syscall_linux_64.go build

* Thu Nov 17 2022 yangjiaqi<yangjiaqi16@huawei.com> - 0.3-29
- fix source

* Mon Oct 24 2022 wuzx<wuzx1226@qq.com> - 0.3-28
- Add sw64 architecture

* Mon Oct 17 2022 vegbir <yangjiaqi16@huawei.com> - 0.3-27
- add dt-test

* Wed Aug 17 2022 vegbir <yangjiaqi16@huawei.com> - 0.3-26
- retry 10 times to avoid isulad unavailable

* Thu Sep 02 2021 zhangsong234 <zhangsong34@huawei.com> - 0.3-25
- enable external linkmode for cgo build

* Mon Feb 08 2021 zhangsong234 <zhangsong34@huawei.com> - 0.3-24
- update version to 0.3-24

* Mon Sep 07 2020 wangkang101 <873229877@qq.com> - 0.3-2
- modify url of source0

* Fri Jul 03 2020 Zhangsong <zhangsong34@huawei.com> - 0.3-1
- release version 0.3
